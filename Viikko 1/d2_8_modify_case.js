/*
Modify case
Create a program that takes in a string, and modifies the every letter of that string to upper case or lower case, depending on the input
example: node .\modifycase.js lower "Do you LIKE Snowmen?" -> do you like snowmen
example: node .\modifycase.js upper "Do you LIKE Snowmen?" -> DO YOU LIKE SNOWMEN NOTE remember to take in the 2nd parameter with quotation marks
*/

const args = (process.argv).slice(2);
let input = args[1];

// Jos syöte loppuu kysymysmerkkiin, poistetaan se
if (input[input.length-1] === "?") {
    input = input.substring(0, input.length-1);
}

if (args[0] === "lower") {
    console.log(input.toLowerCase());
} else if (args[0] === "upper") {
    console.log(input.toUpperCase());
}