// Tehtävä 1: Discounted price
const price = 10;
const discount = 0.2;
console.log(price*discount);

// Tehtävä 2: Travel time
const distance = 25;
const speed = 100;
console.log(distance/speed);

// Tehtävä 3: Seconds in a year
const days = 365;
const hours = 24;
const seconds = 60;
const secondsInYear = days * hours * seconds;
console.log(secondsInYear);

// Tehtävä 4: Area of square
const a = process.argv[2];

// Hyväksytään vain nollaa suurempi numero. Ei hyväksytä tyhjää.
if (a && !Number.isNaN(a) && a > 0) {
    console.log(`A square with sides of length ${a} is ${a**2} square meters in area.`);
} else {
    console.log("Virhe annetussa parametrissa.");
}



