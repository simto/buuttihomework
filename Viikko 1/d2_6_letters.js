/*
Create a program that takes in 3 names and outputs only initial letters of those name separated with dot.
example: node .\initialLetters.js Jack Jake Mike -> j.j.m
*/

// Vaaditaan tasan 3 parametria
if (process.argv.length !== 5) {
    process.exit(1);
}

let initialLetters = [];

for (let i = 2; i < process.argv.length; i++) {
    // Koko nimi
    let nimi = process.argv[i];
    // Nimen alkukirjain, pienenä
    let initial = nimi.substring(0,1).toLowerCase();
    // Lisätään tauluun
    initialLetters.push(initial); 
}
// Tulostetaan nimikirjaimet pisteellä erotettuna
console.log(initialLetters.join("."));