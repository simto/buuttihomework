const readLine = require("readline");

let rl = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Kuinka mones kävijä tänään? ", (visitorCount) => {
    // Joka 2000. kävijä saa lahjakortin. Kävijämäärän on oltava suurempi kuin 0 => poistaa tyhjän vastauksen
    console.log(visitorCount);
    if ((visitorCount > 0) && (visitorCount % 2000 === 0)) {
        console.log("Saa lahjakortin!!!");
    } 
    // Tuhannen kävijän jälkeen joka 25. kävijä saa ilmapallon, paitsi jos on jo saanut lahjakortin
    else if (visitorCount >= 1000 && visitorCount % 25 === 0) {
        console.log("Saa ilmapallon!");
    } else {
        console.log("Ei saa mitään. :(");
    }
    rl.close();
});