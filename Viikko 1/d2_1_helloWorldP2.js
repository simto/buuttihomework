const language = parseInt(process.argv[2]);

function helloWorld2(lang) {
    // Default
    let output = "Hello world!";
    switch(lang) {
        case "fi":
            output = "Hei maailma!";
            break;
        case "es":
            output = "Hola mundo!";
            break;
        default:
            output = "Hello world!";
    }
    console.log(output);
}

helloWorld2(language);

