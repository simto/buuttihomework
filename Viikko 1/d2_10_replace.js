/*
Replace characters (difficult)
Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.
example: node .\replacecharacters.js g h "I have great grades for my grading" -> I have hreat hrades for my hrading
*/

const args = (process.argv).slice(2);
// Argumentit omiksi muuttujikseen
const charToBeChanged = args[0];
const charToBeChangedTo = args[1];
const stringToBeReplaced = args[2];

// Tapa tehdä replace ilman muuttujia:
// const changedString = stringToBeReplaced.replace(/g/g, "h");

// Helpoin tapa toteuttaa tämä tehtävä:
// let changedString = stringToBeReplaced.replaceAll(charToBeChanged, charToBeChangedTo);

// Luodaan regexp-olio, jolla voidaan muuttaa string. "g"-parametri, jotta kaikki merkkijonossa olevat osamerkkijonot muutetaan
const reg = new RegExp(charToBeChanged, "g");
const changedString = stringToBeReplaced.replace(reg, charToBeChangedTo);

console.log(changedString);
