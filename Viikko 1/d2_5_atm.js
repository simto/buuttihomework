/*
ATM
Create a ATM program to check your balance. Create variables balance , isActive , checkBalance . Write conditional statement that implements the flowchartbelow.
Change the values of balance , checkBalance , and isActive to test your code!
*/

let balance = 100;
let checkBalance = true;
let isActive = true;

// Function for checking account balance and active-status
// Conditions checked in order:
// - If account is not active
// - If account is empty
// - If balance is negative
// - Else: print balance
function checkAccountBalance () {
    if (!isActive) {
        console.log("Your account is not active");
        return;
    } else if (balance === 0) {
        console.log("Your account is empty");
        return;
    } else if (balance < 0) {
        console.log("Your balance is negative: " + balance);
        return;
    } else {
        console.log("Print out balance: " + balance);
    }
    return;
}

// USING ATM - Main program / function
function atmMain() {
    console.log("Using ATM");
    if (checkBalance) {
        checkAccountBalance();
    } else {
        console.log("Have a nice day!");
    }
}

atmMain();