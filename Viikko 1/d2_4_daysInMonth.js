/*
How many days
Create a program that takes in a number from commandline that represents month of the year. Use console.log to show how many days there are in the givenmonth number.
*/

// Huom! Ohjelma ei huomioi karkausvuotta. En käytä tässä valmiita päivämäärään liittyviä funktioita, koska tehtävänanto ei kata vuosilukua.
const month = parseInt(process.argv[2]);
let daysInMonth = 0;

// Jos parametria ei ole annettu keskeytetään ohjelma
if (process.argv.length <= 2) {
    process.exit(1);
}
// Annetun kuukauden oltava välillä 1-12
if ((month < 1) || (month > 12)) {
    process.exit(1);
}

switch (month) {
    case 4:
    case 6: 
    case 9:
    case 11:
        daysInMonth = 30;
        break;
    case 2:
        daysInMonth = 28;
        break;
    default:
        daysInMonth = 31;
}
console.log(daysInMonth);