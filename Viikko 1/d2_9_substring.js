/*
Annoying substring
Create a program that takes in a string and drops off the last word of any given string, and console.logs it out.
example: node .\annoyingSubstring.js "Hey I'm alive!" -> Hey I'm
*/

const args = (process.argv).slice(2);
const stringToBeChanged = args[0];


// Solution 1: does not work if there is " " (space) at the end of the string. 
//             or string ends with exclamation / question and there is space before that.

// Find the index of last " " of the string. (Marks the beginning of the last word of the string.)
const lastIndex = stringToBeChanged.lastIndexOf(" ");
// Create substring including everything but the last word (and the " " before it)
const newString = stringToBeChanged.substring(0, lastIndex)


// Solution 2: Using regular expression.
//             Does not count "?" or "!" or any single letter to be a work. Works correctly with spaces.
//             Problems: scandic letters
const newString2 = stringToBeChanged.replace(/[\W]*\S+[\W]*$/, '');

console.log(newString);
console.log(newString2);
