/*
Greater, smaller or equal
1. Create a program that takes in two numbers a and b from the command line.
2. Print out "a is greater" if a is bigger than b, and vice versa, and "they are equal" if they are equal
3. Modify program to take in a third string argument c, and print out "yay, you guessed the password", if a and b are equal AND c is "hello world"
Remember to test that the program outputs the right answer in all cases.
*/

// argv on string, joten muutetaan integeriksi
const int1 = parseInt(process.argv[2]);
const int2 = parseInt(process.argv[3]);

// Mahdollinen 2-sanainen salasana, saadaan kahden eri parametrin argv[4] ja argv[5] arvoista.
// Tämä ratkaisu ei huomioi parametreissa olevia ylimääräisiä välilyöntejä.
const password = process.argv[4] + " " + process.argv[5];

function compare(a, b, c) {
    // Yhtäsuuruuden vertailu + salasanan testaus
    if (a === b) {
        if (c === "hello world") {
            return "yay, you guessed the password";
        } else {
            return "they are equal";
        }
    }
    // Erisuuruuden vertailu
    if (a > b) {
        return "a is greater";
    } else {
        return "b is greater"
    }
    
}
console.log(compare(int1, int2, password));

