/*
String length comparison
Create a program that takes in 3 names, and compares the length of those names. Print out the names ordered so that the longest name is first.
example: node .\lengthcomparison.js Maria Joe Philippa -> Philippa Maria Joe
*/

// Vaaditaan tasan 3 parametria
if (process.argv.length !== 5) {
    process.exit(1);
}

// Laitetaan nimet ja niiden pituudet taulukkoon järjestelyä varten.
const names = [
    [process.argv[2], process.argv[2].length],
    [process.argv[3], process.argv[3].length],
    [process.argv[4], process.argv[4].length],
];

// Järjestellään taulu suurimmasta pienimpään.
names.sort(function(a, b) {
    return b[1] - a[1];
});

// Laitetaan järjestyksessä olevat nimet uuteen taulukkoon tulostusta varten
let result = [];
for (let i = 0; i < names.length ; i++) {
    result.push(names[i][0]);
}
console.log(result.join(" "));
