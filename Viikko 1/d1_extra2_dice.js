const dice = process.argv[2];
const argumentError = "Please provide the die type. Example: D6";
const defaultDice = 6;

// Jos yhtään parametria ei ole annettu, keskeytetään ohjelma
if (process.argv.length <= 2) {
    console.log(argumentError);
    process.exit(1);
}

// Argumentin loppuosa omaksi muuttujaksi. Pyöristää samalla desimaaliluvut kokonaisluvuksi.
let sides = parseInt(dice.substring(1));

// Tarkistetaan onko annettu parametri oikeaa syntaksia: pitää alkaa "D"
// ja olla numero
if ((dice[0] !== "D") || isNaN(sides) ) {
    console.log(argumentError);
    process.exit(1);
}

// Sivujen lkm pitää olla 3 ja 100 välillä. Jos ei ole, asetetaan oletusarvo 6.
if ((sides < 3) || (sides > 100)) {
    console.log(`Invalid die side count, needs to be between 3 and 100. Setting die to default of D${defaultDice}`);
    sides = 6;
}

// Funktio nopanheitolle: int = arpakuution sivujen määrä.
// Satunnainen numero väliltä 0 - int, pyöristys ylöspäin
// Palautetaan tulos, tuloksen mukaisten tekstien kera
const rollDie = function (int) {
    let text = "The roll is ";
    let roll = Math.ceil(Math.random()*int);
    if (roll === 1) { 
        text = "Ouch! " + text;
    } else if (roll === int) {
        text = "Yippii!!! " + text;
    }
    return text + roll;
};

const readLine = require("readline");
const interface = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
});

const recursiveAsyncReadLine = function () {
    interface.question("Press <enter> for a dice roll, type 'q' + <enter> to exit.", function (answer) {
        if (answer === "q") { // we need some base case, for recursion
            console.log("Bye!");
            return interface.close(); // closing RL and returning from function.
        }    
        console.log(rollDie(sides));
        recursiveAsyncReadLine(); // Calling this function again to ask new question
    });
};
recursiveAsyncReadLine();





