/*
Largest and smallest
From the command line read in three numbers, number_1, number_2 and number_3. Decide their values freely.
Find the
a) largest one
b) smallest one
c) if they all are equal, print that out
console.log() its name and value.
*/

// Vaaditaan tasan 3 parametria
if (process.argv.length !== 5) {
    console.log("Tarvitaan 3 parametria");
    process.exit(1);
}

// Laitetaan parametrit tauluun järjestelyä varten.
const numbers = [
    ["number_1", parseInt(process.argv[2])],
    ["number_2", parseInt(process.argv[3])],
    ["number_3", parseInt(process.argv[4])],
];

// Järjestellään taulu pienimmästä suurimpaan.
numbers.sort(function(a, b) {
    return a[1] - b[1];
});

console.log("Largest: " + numbers[numbers.length-1]);
console.log("Smallest: " + numbers[0]);
// Jos järjestelyn jälkeen pienin ja suurin on sama, kaikki numerot ovat yhtä suuria.
if (numbers[0][1] === numbers[numbers.length-1][1]) {
    console.log("All numbers are equal")
}
console.log(numbers[0][1]);