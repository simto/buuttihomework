import mongoose from "mongoose";
import User from "../models/banking.model.js";
import dotenv from "dotenv";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

dotenv.config();

const saltRounds = 10;


/**
 * Finds user by username from model's database
 * @param {string} username - Name of the user
 * @param {User} model
 * @returns {User}
 */
async function findUserByUsername (username, model) {
    try {
        return model.findOne({"username": username});
    } catch (error) {
        throw new Error("Unable to connect to the database.");
    }
}

/**
 * Checks if given input is numeric.
 * @param {*} n - Input to be checked
 * @returns {boolean}
 */
function isNumber(n) { 
    return !isNaN(parseFloat(n)) && !isNaN(n - 0);
}

/**
 * Validates property "amount" in a request.
 * - amount must be numeric, greater or equal to 0
 * @param {*} req - Request
 * @returns {validationObject}
 */
function validateAmount (req) {
    let validationObject = { isValid: false };
    if (!isNumber(req.body.amount)) {
        validationObject.statuscode = 409;
        validationObject.message = "Amount missing or not numeric.";
        return validationObject;
    }
    if (req.body.amount <= 0) {
        validationObject.statuscode = 409;
        validationObject.message = "Amount must be greater than 0.";
        return validationObject;
    }
    validationObject.isValid = true;
    return validationObject;
}

/**
 * * Validation for property "username" of User model
 * - username must be at least 6 characters
 * - username has to be unique
 * @param {string} username 
 * @returns {validationObject}
 */
async function validateNewUsername (username) {
    let validationObject = { isValid: false };
 
    if (username === undefined || username.length < 6) {
        validationObject.statuscode = 409;
        validationObject.message = "Username must be at least 6 characters.";
        return validationObject;
    }

    let usernameAlreadyInUse;
    try {
        usernameAlreadyInUse = await findUserByUsername(username, User);
    } catch {
        validationObject.statuscode = 404;
        validationObject.message = "Error finding user.";
        return validationObject;
    }
 
    if (usernameAlreadyInUse) {
        validationObject.statuscode = 409;
        validationObject.message = "Username is already taken.";
        return validationObject;
    }
    validationObject.isValid = true;
    validationObject.username = username;
    return validationObject;
}

/**
 * Validation for property "id" of User model
 * @param {string} id 
 * @returns {validationObject}
 */
async function validateUserId (id) {
    let validationObject = { isValid: false };
    
    const userId = id;
    if (userId === undefined) {
        validationObject.statuscode = 409;
        validationObject.message = "User id missing.";
        return validationObject;
    }
    
    let user;
    try {
        user = await User.findById(userId);
        if (user === null) {
            validationObject.statuscode = 404;
            validationObject.message = "User not found.";
            return validationObject;
        }
    } catch {
        validationObject.statuscode = 404;
        validationObject.message = "Error finding user.";
        return validationObject;
    }
    
    validationObject.isValid = true;
    validationObject.user = user;
    return validationObject;
}

/**
 * * Validation for property "password" of User model
 * @param {*} req 
 * @param {User} user 
 * @returns {validationObject}
 */
async function validatePassword (req, user) {
    let validationObject = { isValid: false };
    const password = req.body.password;
    if (password === undefined) {
        validationObject.statuscode = 409;
        validationObject.message = "Password missing.";
        return validationObject;
    }

    let isCorrectPassword;
    try {
        isCorrectPassword = await bcrypt.compare(password, user.password);
    } catch {
        validationObject.statuscode = 401;
        validationObject.message = "Invalid password.";
        return validationObject;    
    }

    if (!isCorrectPassword) {
        validationObject.statuscode = 401;
        validationObject.message = "Invalid password.";
        return validationObject;
    }
    validationObject.isValid = true;
    validationObject.user = user;
    return validationObject;
}


/**
 * Connect to database
 */
export const connectMongoose = () => {
    try {
        mongoose.connect(
            "mongodb://127.0.0.1/bankdb",
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true,
                user: process.env.MONGODB_USER,
                pass: process.env.MONGODB_PASS
            }
        );
        console.log("connected");
    } catch (err) {
        console.log(err);
    }
};

/**
 * Creates new user. Checks is username already exits and if password is too short
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const postUser = async (req, res) => {
    const usernameValidation = await validateNewUsername(req.body.username);
    if (!usernameValidation.isValid) {
        return res.status(usernameValidation.statuscode).json(usernameValidation.message);
    }

    const password = req.body.password;
    if (password === undefined || password.length < 8) {
        return res.status(400).json("Password must be at least 8 characters.");
    }
    const pwHash = await bcrypt.hash(req.body.password, saltRounds);

    if (!isNumber(req.body.balance)) {
        return res.status(409).json({message: "Balance missing or not numeric."});
    }

    const balance = (req.body.balance === undefined) ? 0 : req.body.balance;

    const newUser = new User({username: usernameValidation.username, password: pwHash, balance: balance});
    newUser.save()
        .then(result => {
            //res.status(200).send(`Car (id: ${result._id}) added.`);
            res.status(200).json(result.id);
        })
        .catch(err => {
            res.status(500).json(err);
        });
};

/**
 * Get users account balance
 * @param {*} req 
 * @param {*} res 
 */
export const getBalance = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        res.status(200).json(user.balance);
    } catch (err) {
        res.status(404).json({message: "User not found."});
    }
};

/**
 * Withdraw money from user('s account)
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const withdrawMoney = async (req, res) => {
    const amountValidation = validateAmount(req);
    if (!amountValidation.isValid) {
        return res.status(amountValidation.statuscode).json(amountValidation.message);
    }
    
    const userIdValidation = await validateUserId(req.user.userid);
    if (!userIdValidation.isValid) {
        return res.status(userIdValidation.statuscode).json(userIdValidation.message);
    }
    let validatedUser = userIdValidation.user;
                
    const newBalance = validatedUser.balance - req.body.amount;
    if (newBalance < 0) {
        return res.status(409).json({message: "Insufficient funds. Account balance: " + validatedUser.balance });
    }
    
    User.findByIdAndUpdate(validatedUser.id, {balance: newBalance})
        .then(() => {
            res.status(200).json(newBalance);
        })
        .catch(err => {
            res.status(500).send(err);
        });
};

/**
 * Deposit money to user('s account)
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const depositMoney = async (req, res) => {
    const amountValidation = validateAmount(req);
    if (!amountValidation.isValid) {
        return res.status(amountValidation.statuscode).json(amountValidation.message);
    }
        
    const userIdValidation = await validateUserId(req.user.userid);
    if (!userIdValidation.isValid) {
        return res.status(userIdValidation.statuscode).json(userIdValidation.message);
    }
    let validatedUser = userIdValidation.user;
   
    const newBalance = validatedUser.balance + req.body.amount;
        
    User.findByIdAndUpdate(validatedUser.id, {balance: newBalance})
        .then(() => {
            res.status(200).json(newBalance);
        })
        .catch(err => {
            res.status(500).send(err);
        });
};

/**
 * Transfer money from one user to another
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const transferMoney = async (req, res) => {    
    const amountValidation = validateAmount(req);
    if (!amountValidation.isValid) {
        return res.status(amountValidation.statuscode).json(amountValidation.message);
    }
        
    const userIdValidation = await validateUserId(req.user.userid);
    if (!userIdValidation.isValid) {
        return res.status(userIdValidation.statuscode).json(userIdValidation.message);
    }
    let validatedUser = userIdValidation.user;

    // Check if recipient exits
    const recipientIdValidation = await validateUserId(req.body.recipient_id);
    if (!recipientIdValidation.isValid) {
        return res.status(recipientIdValidation.statuscode).json(recipientIdValidation.message);
    }
    const validatedRecipient = recipientIdValidation.user;
    console.log(validatedUser.id);
    console.log(validatedRecipient.id);
    // Recipient must be different that sender
    if (validatedUser.id === validatedRecipient.id) {
        return res.status(409).json("Recipient must be different than sender.");
    }

    const newBalance = validatedUser.balance - req.body.amount;
    if (newBalance < 0) {
        return res.status(409).json({message: "Insufficient funds. Account balance: " + validatedUser.balance });
    }
    
    const newRecipientBalance = validatedRecipient.balance + req.body.amount;

    // Update balance for transferer    
    User.findByIdAndUpdate(validatedRecipient.id, {balance: newRecipientBalance})
        .then(() => {
            // Update balance for recipients
            User.findByIdAndUpdate(validatedUser.id, {balance: newBalance})
                .then(() => {
                    res.status(200).json(newBalance);
                })
                .catch(err => {
                    res.status(500).send(err);
                });
        })
        .catch(err => {
            res.status(500).send(err);
        });  
};

/**
 * Change user's username
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const updateUsername = async (req, res) => {
    const userIdValidation = await validateUserId(req.user.userid);
    if (!userIdValidation.isValid) {
        return res.status(userIdValidation.statuscode).json(userIdValidation.message);
    }
    const validatedUser = userIdValidation.user;

    const usernameValidation = await validateNewUsername(req.body.new_name);
    if (!usernameValidation.isValid) {
        return res.status(usernameValidation.statuscode).json(usernameValidation.message);
    }
    const validNewUsername = usernameValidation.username;

    User.findByIdAndUpdate(validatedUser.id, {username: validNewUsername})
        .then(() => {
            res.status(200).json(validNewUsername);
        })
        .catch(err => {
            res.status(500).send(err);
        });
};

/**
 * Change user's password
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const updatePassword = async (req, res) => {
    const userIdValidation = await validateUserId(req.user.userid);
    if (!userIdValidation.isValid) {
        return res.status(userIdValidation.statuscode).json(userIdValidation.message);
    }
    const validatedUser = userIdValidation.user;

    const password = req.body.new_password;
    if (password === undefined || password.length < 8) {
        return res.status(400).json("Password must be at least 8 characters.");
    }
    const newPwHash = await bcrypt.hash(password, saltRounds);

    User.findByIdAndUpdate(validatedUser.id, {password: newPwHash})
        .then(() => {
            res.status(200).json(password);
        })
        .catch(err => {
            res.status(500).send(err);
        });
};
/**
 * Login user
 * @param {*} req 
 * @param {*} res 
 */
export const loginUser = async (req, res) => {
    const userIdValidation = await validateUserId(req.body.id);
    if (!userIdValidation.isValid) {
        return res.status(userIdValidation.statuscode).json(userIdValidation.message);
    }
    const validatedUser = userIdValidation.user;
       
    const isCorrectPassword = await bcrypt.compare(req.body.password, validatedUser.password);

    if(isCorrectPassword) {
        const token = jwt.sign (
            { userid : req.body.id },
            process.env.JWT_SECRET,
            { expiresIn: "1h"}
        );    
        res.status(200).send({token});
    } else {
        res.status(401).send("Invalid username or password");
    }
};