import express from "express";
import { connectMongoose } from "./controllers/banking.controller.js";
import bankRouter from "./routes/banking.routes.js";

export const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/bank", bankRouter);

connectMongoose();

app.listen(5000, () => {
    console.log("Listening to 5000");
});