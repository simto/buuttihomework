import * as BankController from "../controllers/banking.controller.js";
import express from "express";
import jwt from "jsonwebtoken";

const router = express.Router();

/**
 * Operations allowed without authentication
 */
router.post("/user",BankController.postUser);
router.get("/:id/balance",BankController.getBalance);
router.post("/login", BankController.loginUser);

/**
 * Authentication
 */
router.use((req, res, next) => {
    const auth = req.get("authorization");
    if (auth && auth.toLowerCase().startsWith("bearer ")) {
        const token = auth.substring(7);
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
        if(!token || !decodedToken.userid) {
            return res.status(401).json({error: "token missing or invalid"});
        }
        req.user = decodedToken;
        next();
    } else {
        return res.status(401).json({error: "token missing or invalid"});
    }
});

router.patch("/user/withdraw",BankController.withdrawMoney);
router.patch("/user/deposit",BankController.depositMoney);
router.patch("/transfer",BankController.transferMoney);
router.patch("/update",BankController.updateUsername);
router.patch("/user/password",BankController.updatePassword);

export default router;

