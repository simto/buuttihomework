import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        minlength: 8
    },  
    balance: {
        type: Number,
        required: true,
        min: 0,
    }
});

const User = mongoose.model("users", UserSchema);
export default User;
