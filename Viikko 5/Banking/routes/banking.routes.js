import * as BankController from "../controllers/banking.controller.js";
import express from "express";

const router = express.Router();

router.post("/user",BankController.postUser);
router.get("/:id/balance",BankController.getBalance);
router.patch("/user/withdraw",BankController.withdrawMoney);
router.patch("/user/deposit",BankController.depositMoney);
router.patch("/transfer",BankController.transferMoney);
router.patch("/update",BankController.updateUsername);
router.patch("/user/password",BankController.updatePassword);

export default router;

