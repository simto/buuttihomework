import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    username: String,
    password: String,
    balance: Number,
});

const User = mongoose.model("users", UserSchema);
export default User;
