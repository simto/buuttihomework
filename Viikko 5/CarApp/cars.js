import Car from "./models/cars.model.js";
import * as CarsController from "./controllers/cars.controller.js";
import carRouter from "./routes/cars.routes.js";
import mongoose from "mongoose";
import dotenv from "dotenv";
import express from "express";

dotenv.config();

async function connectMongoose() {
    try {
        await mongoose.connect(
            "mongodb://127.0.0.1/carsdb",
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true,
                user: process.env.MONGODB_USER,
                pass: process.env.MONGODB_PASS
            }
        );
    } catch (err) {
        console.log(err);
    }
}

connectMongoose();
CarsController.importFromJson("./user_cars.json", Car);

export const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(carRouter);

app.listen(5000, () => {
    console.log("Listening to 5000");
});
