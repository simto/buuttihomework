import * as CarsController from "../controllers/cars.controller.js";
import express from "express";

const router = express.Router();

router.get("/userCar/:id", CarsController.getCar);
router.post("/usedCar", CarsController.postCar);

router.get("/userCar", CarsController.getAllCars);

export default router;

