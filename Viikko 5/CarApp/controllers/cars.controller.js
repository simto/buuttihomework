import Car from "../models/cars.model.js";
import fs from "fs";

/**
 * Reads JSON [{},{},{},...,{}] from a file and imports data to Mongo database
 * @param {*} jsonPath Path of JSON file
 * @param {*} model Data model of database
 */
export const importFromJson = (jsonPath, model) => {
    let json;
    try {
        json = JSON.parse(fs.readFileSync(jsonPath, "utf-8"));
    } catch {
        json = [];
    }
    model.insertMany(json)
        .then(
            console.log("Data inserted")
        )
        .catch(err => {
            console.log(err);
        });
};

export const getAllCars = (req, res) => {
    Car.find()
        .then(result => {
            res.status(200).send(result);
        })
        .catch(err => {
            res.status(500).send(err);
        });
};

export const getCar = async (req, res) => {
    try {
        const car = await Car.findById(req.params.id);
        res.send(car);
    } catch (err) {
        res.status(500).send(err);
    }
};

export const postCar = (req, res) => {
    const newCar = new Car(req.body);
    
    newCar.save()
        .then(result => {
            //res.status(200).send(`Car (id: ${result._id}) added.`);
            res.status(200).json(result.id);
        })
        .catch(err => {
            res.status(500).send(err);
        });
};