/* 
    4. Count vowels
    Return the number (count) of vowels in the given string. Let's consider a, e, i, o, u, y as vowels in this exercise.
    getVowelCount('abracadabra') // 5 
*/

function getVowelCount (string) {
    const vowels = ["a", "e", "i", "o", "u", "y", "å", "ä", "ö"];
    let vowelCount = 0;
    // Go though string, letter by letter, check if it is vowel (matching array "vowels").
    for (let letter of string.toLowerCase()) {
        if (vowels.includes(letter)) {
            vowelCount++;
        }
    }
    return vowelCount;
}
console.log(getVowelCount("abracadabra")); // 5