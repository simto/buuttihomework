/* 
    1. Calculator
    Create a function that takes in an operator and two numbers and returns the result.
    function calculator(operator, num1, num2) {
    // your code
    };
    calculator("+", 2, 3);
    For example, if the operator is "+", sum the given numbers. In addition to sum, calculator can also calculate differences, multiplications and divisions. If the
    operator is something else, return some error message like "Can't do that!" 
*/

function calculator(operator, num1, num2) {
    switch (operator) {
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "*":
            return num1 * num2;
        case "/":
            return num1 / num2;
        default:
            return "Can't do that!"
      }
};

console.log(calculator("+", 3, 5));
console.log(calculator("-", 3, 5));
console.log(calculator("*", 3, 5));
console.log(calculator("/", 3, 5));
console.log(calculator("abc", 3, 5));