/* 
    2. Likes
    Create a "like" function that takes in a array of names. Depending on a number of names (or length of the array) the function must return strings as follows:
    likes([]); // "no one likes this"
    likes(["John"]) // "John likes this"
    likes(["Mary", "Alex"]) // "Mary and Alex like this"
    likes(["John", "James", "Linda"]) // "John, James and Linda like this"
    likes(["Alex", "Linda", "Mark", "Max"]) // must be "Alex, Linda and 2 others
    For 4 or more names, "2 others" simply increases. 
*/

function likes (names) {
    switch (names.length) {
        case 0:
            return "No one likes this."
        case 1:
            return `${names[0]} likes this.`
        case 2:
            return `${names[0]} and ${names[1]} like this.`
        case 3:
            return `${names[0]}, ${names[1]} and ${names[2]} like this.`
        default:
            return `${names[0]}, ${names[1]} and ${names.length-2} others like this.`
      }
}

console.log(likes(["Simo"]));
console.log(likes(["Simo", "Seppo"]));
console.log(likes(["Simo", "Seppo", "Saara"]));
console.log(likes(["Simo", "Seppo", "Saara" , "Sini"]));
console.log(likes(["Simo", "Seppo", "Saara" , "Sini", "Simo", "Seppo", "Saara" , "Sini"]));

