/* 
    6. Generate username and password
  
    Create a function (or multiple functions) that generates username and password from given firstname and lastname.
    Username: B + last 2 numbers from current year + 2 first letters from both last name and first name in lower case
    Password: 1 random letter + first letter of first name in lowercase + last letter of last name in uppercase + random special character + last 2 numbers from
    current year
    Example: John Doe -> B20dojo, mjE(20
    generateCredentials("John", "Doe")
    Get to know to ASCII table. Random letters and special characters can be searched from ASCII table with String.fromCharCode() method with indexes. For
    example String.fromCharCode(65) returns letter A.
    Hints: Generate random numbers (indexes) to get one random letter and one special character. Use range of 65 to 90 to get the LETTER and 33 to 47 to
    get the SPECIAL CHARACTER. Use build-in function to get the current year
    Output: “username: username, password: password”
    Links: http://www.asciitable.com/ 
*/

// Random number between min and max (both inclusively)
function randomInt (min, max) {
    const range = max - min + 1;
    return Math.floor(min + Math.random() * range);    
}

function generateCredentials (firstName, lastName) {

    // Generate username
    const currentYear = new Date().getFullYear();
    const lastTwoDigitsOfYear = currentYear.toString().slice(-2);
    // Username = "B" + 2 first letter of lastname + 2 first letter of firstname + last 2 digits of current year 
    const userName = "B" + lastName.slice(0,2) + firstName.slice(0,2) + lastTwoDigitsOfYear;

    // Generate password
    const randomCharCode = randomInt(65, 90);
    const randomSpecialCharCode = randomInt(33, 47);
    const randomChar = String.fromCharCode(randomCharCode);
    const randomSpecialChar = String.fromCharCode(randomSpecialCharCode);
    // Password : 1 random char + first letter of firstname in lowercase + last letter of lastname in uppercase + random special char + last 2 numbers from current year
    const passWord = randomChar + firstName.toLowerCase().slice(0,1) + lastName.toUpperCase().slice(-1) + randomSpecialChar + lastTwoDigitsOfYear;

    return `username: ${userName}, password: ${passWord}`
}

const joe = generateCredentials("John", "Doe");
console.log(joe);

const seppo = generateCredentials("Seppo", "Merilokki");
console.log(seppo);