/* 
    5. Check the exam
    The first input array is the key to the correct answers to an exam, like ["a", "a", "b", "d"] . The second one contains a student's submitted answers.
    The two arrays are not empty and are the same length. Return the score for this array of answers, giving +4 for each correct answer, -1 for each
    incorrect answer, and +0 for each blank answer, represented as an empty string.
    If the score < 0, return 0
    For example:
    checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"]) → 6
    checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""]) → 7
    checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"]) → 16
    checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"]) → 0
*/

function checkExam (correctAnswers, answers) {
    const pointsPerCorrectAnswer = 4;
    const pointsPerIncorrectAnswer = -1;
    const pointPerBlankAnswer = 0;

    let score = 0;

    // Check each correct answer against given answer with same index.
    // Modify score accordingly.
    correctAnswers.forEach((answer, index) => {
        if (answers[index] !== "") {
            score = (answers[index] === answer) ? score += pointsPerCorrectAnswer : score += pointsPerIncorrectAnswer;
        } else {
            score += pointPerBlankAnswer;
        }
    });
    return (score < 0) ? 0 : score; // If score is lower than 0, score is 0.
};

const a = checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"]);
const b = checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""]);
const c = checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"]);
const d = checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"]);

console.log(a, b, c, d);