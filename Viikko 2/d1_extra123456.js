/* 
Use the built-in .reduce() method on arrays to solve all of these problems
Feel free to copy and paste the code for easy testing.
*/

// ------------------------------------------------------------
// 1) Turn an array of numbers into a total of all the numbers.
// ------------------------------------------------------------

function total(arr) {
    return arr.reduce((acc, curr) =>  acc + curr, 0); // Does not matter if initial value is given
}
console.log("Tehtävä 1:");
console.log(total([1,2,3])); // 6


// --------------------------------------------------------------------
// 2) Turn an array of numbers into a long string of all those numbers.
// --------------------------------------------------------------------

function stringConcat(arr) {
    let string = arr.reduce((acc, curr) => {
        return acc.toString() + curr.toString();
    }); // No need for initial value. It could be "", but not 0, as it will convert to string.
    return string;
}
console.log("Tehtävä 2:");
console.log(stringConcat([1,2,3])); // "123"

// -----------------------------------------------------------------------
// 3) Turn an array of voter objects into a count of how many people voted
// Note: You don't necessarily have to use reduce for this, so try to think of multiple ways you could solve this.
// -----------------------------------------------------------------------


function totalVotes(arr) {
    let voteCount = arr.reduce((acc, curr) => {
        /* Longer version
        if (curr.voted) { 
            return acc + 1;
        } else {
            return acc;
        } */
        return (curr.voted) ? acc + 1 : acc;
    }, 0); // This has to have initial value. 
    return voteCount;
}

const voters = [
    {name: "Bob", age: 30, voted: true},
    {name: "Jake", age: 32, voted: true},
    {name: "Kate", age: 25, voted: false},
    {name: "Sam", age: 20, voted: false},
    {name: "Phil", age: 21, voted: true},
    {name: "Ed", age:55, voted: true},
    {name: "Tami", age: 54, voted: true},
    {name: "Mary", age: 31, voted: false},
    {name: "Becky", age: 43, voted: false},
    {name: "Joey", age: 41, voted: true},
    {name: "Jeff", age: 30, voted: true},
    {name: "Zack", age: 19, voted: false}
];
console.log("Tehtävä 3:");
console.log(totalVotes(voters)); // 7

// ** Make array of all age -values, from array "voters"
// const ages = voters.map(voter => voter.age);
// console.log(ages);


// --------------------------------------------------------------------------------------------------------------
// 4) Given an array of all your wishlist items, figure out how much it would cost to just buy everything at once
// --------------------------------------------------------------------------------------------------------------

function shoppingSpree(arr) {
    let total = arr.reduce((acc, curr) => {
        return acc + curr.price;
    }, 0); // This has to have initial value (of 0)
    return total;
}

const wishlist = [
    { title: "Tesla Model S", price: 90000 },
    { title: "4 carat diamond ring", price: 45000 },
    { title: "Fancy hacky Sack", price: 5 },
    { title: "Gold fidgit spinner", price: 2000 },
    { title: "A second Tesla Model S", price: 90000 }
];
console.log("Tehtävä 4:");
console.log(shoppingSpree(wishlist)); // 227005


// -------------------------------------------------------------
// 5) Given an array of arrays, flatten them into a single array
//    Note: Take a look at Array.concat() to help with this one
// -------------------------------------------------------------

function flatten(arr) {
    let array = arr.reduce((acc, curr) => {
        return acc.concat(curr);
    }); // No need for initial value. It could be []
    return array;
}

const arrays = [
    ["1", "2", "3"],
    [true],
    [4, 5, 6]
];
console.log("Tehtävä 5:");
console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6];


//--------------------------------------------------------------------------------------------------------------------------------
// 6) Given an array of potential voters, return an object representing the results of the vote
//    Include how many of the potential voters were in the ages 18-25, how many from 26-35, how many from 36-55, 
//    and how many of each of those age ranges actually voted. The resulting object containing this data should have 6 properties. 
//    See the example output at the bottom.
//--------------------------------------------------------------------------------------------------------------------------------

const voters2 = [
    {name: "Bob", age: 30, voted: true},
    {name: "Jake", age: 32, voted: true},
    {name: "Kate", age: 25, voted: false},
    {name: "Sam", age: 20, voted: false},
    {name: "Phil", age: 21, voted: true},
    {name: "Ed", age: 55, voted: true},
    {name: "Tami", age: 54, voted: true},
    {name: "Mary", age: 31, voted: false},
    {name: "Becky", age: 43, voted: false},
    {name: "Joey", age: 41, voted: true},
    {name: "Jeff", age: 30, voted: true},
    {name: "Zack", age: 19, voted: false}
];

function voterResults(arr) {
    // For voter results.
    const votersResultsObject =  {
        numYoungVotesPeople: 0,
        numYoungPeople: 0,
        numMidVotesPeople: 0,
        numMidsPeople: 0,
        numOldVotesPeople: 0,
        numOldsPeople: 0
    };
    
    return arr.reduce((votersObject, voter) => {
        if (voter.age >= 18 && voter.age <= 25) {
            // Update people counter for this age group
            votersObject.numYoungPeople += 1;
            // If person in this age group voted, increase corresponding vote count in votersObject
            if (voter.voted) votersObject.numYoungVotesPeople += 1;
        } else if (voter.age >= 26 && voter.age <= 35) {
            votersObject.numMidsPeople += 1;
            if (voter.voted) votersObject.numMidVotesPeople += 1;
        } else if (voter.age >= 36 && voter.age <= 55) {
            votersObject.numOldsPeople += 1;
            if (voter.voted) votersObject.numOldVotesPeople += 1;   
        }
        return votersObject;
    }, votersResultsObject); // Initial object is already created before.
}
console.log("Tehtävä 6:");
console.log(voterResults(voters2)); // Returned value shown below:

/*
{ numYoungVotes: 1,
  numYoungPeople: 4,
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numOldVotesPeople: 3,
  numOldsPeople: 4 
}
*/