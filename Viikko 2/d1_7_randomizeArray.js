/* 
    Randomize order of array
    const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
    Create a program that every time you run it, prints out an array with differently randomized order of the array above.
    Example:
    node .\randomizeArray.js -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10] 
*/

const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

// Generate a random number (i), within the index range of an array
// store value from index (i) to new array and remove that value/index from the old table (splice)
// i--, start over

// Random number between min and max (both inclusively)
function randomInt (min, max) {
    const range = max - min + 1;
    return Math.floor(min + Math.random() * range);    
}

function randomizeArray (array) {
    
    const randomizedArray = [];

    // As long as there are elements in array
    while (array.length > 0) {
        // Random index to be removed, within a range of indexes in current array
        const indexToBeRemoved = randomInt(0, array.length-1);
        // Add random element from given array to new array, remove the old element from original array.
        randomizedArray.push(array.splice(indexToBeRemoved, 1)[0]);
     }
    return randomizedArray;
}
console.log(randomizeArray(array));