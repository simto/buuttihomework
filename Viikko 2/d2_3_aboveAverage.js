/* 
    3. Above average
    
    Create a function that takes in an array and returns a new array of values that are above average.
    aboveAverage([1, 5, 9, 3]) // outputs an array that has values greater than 4.5
*/

function aboveAverage (valuesArray) {
    // Using reduce to sum all values in array
    const sumOfValues = valuesArray.reduce((total, value) => total + value, 0);
    // Calculate average of all values in array
    const averageValue = sumOfValues / valuesArray.length;
    // Create new array from values that are above average
    const aboveAverage = valuesArray.filter(value => value > averageValue);
    // Array of above average values
    return aboveAverage;    
}

console.log(aboveAverage([1, 5, 9, 3])); // outputs an array that has values greater than 4.5