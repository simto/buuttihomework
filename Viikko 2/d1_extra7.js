/* 
    1. Write a function that takes a string and returns an object representing the character count for each
    letter. Use .reduce to build this object. ex. countLetters('abbcccddddeeeee') // => {a:1, b:2, c:3, d:4,
    e:5}
    var countLetters = function(string){
        // your code here
    }; 
*/

// ['a', 'b', 'b', 'c', 'c', 'c', 'd', 'd', 'd', 'd', 'e', 'e', 'e', 'e', 'e']

const letters = "abbcccddddeeeee";
const arrayOfLetters = letters.split("");
const letterCount = arrayOfLetters.reduce((total, letter) => { 
    if (total[letter]) {    // If object for current letter already exists, add +1 to it's value.
        total[letter]++;
    } else {
        total[letter] = 1;  // If object for current letter does not exist, create object for it, with value of 1
    }
    return total;           // Return 
}, {});
console.log(letterCount); // { a: 1, b: 2, c: 3, d: 4, e: 5 }

/*
const letterCount = arrayOfLetters.reduce((total, letter) => { 
    return total[letter] ? ++total[letter] : total[letter] = 1, total;
}, {});
    
     Optio 2:
    total[letter] ? total[letter]++ : total[letter] = 1;
    return total; 
*/