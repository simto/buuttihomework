/* 
    2. Write a function that takes a string and a target, and returns true or false if the target is present in
    the string. Use .reduce to acomplish this. ex. isPresent('abcd', 'b') // => true ex. isPresent('efghi', 'a') //
    => false
    var isPresent = function(string, target) {
    // your code here
    } 
*/

function isPresent (string, target) {
    // Convert string into array
    const stringArray = string.split("");
    // isPresent will either have value of false or true
    const isPresent = stringArray.reduce((isFound, currValue) => {
        // If target is found, isFound = true. If after that target is not found, isFound still remains the same, this way it wont change back to false once it's true.
        return (currValue === target)  ? true : isFound = isFound;
    }, false);
    return isPresent;
}
console.log(isPresent("abcd", "b"));
console.log(isPresent("efghi", "a"));