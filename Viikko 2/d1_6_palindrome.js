/*
    isPalindrome
    Check if given string is a palindrome.
    Examples:
    node .\checkPalindrome.js saippuakivikauppias -> Yes, 'saippuakivikauppias' is a palindrome
    node .\checkPalindrome.js saippuakäpykauppias -> No, 'saippuakäpykauppias' is not a palindrome
*/

// Get sentence string as argument
const sentence = process.argv[2];

// Function to reverse a given string
function reverseWord (word) {
    // 1. split each word into array, 2. reverse array, 3. join array into string
    return word.split("").reverse().join("");
}

function checkPalindrome (string) {
    if (string === (reverseWord(string))) {
        console.log(`Yes, '${string}' is a palindrome`);
    } else {
        console.log(`No, '${string}' is not a palindrome`);
    }
}

checkPalindrome(sentence);


