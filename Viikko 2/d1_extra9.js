/* 
    3. Write a function decode that will take a string of number sets and decode it using the following rules:
    When each digit of each set of numbers is added together, the resulting sum is the ascii code for a
    single letter.
    Convert each set of numbers into a letter and discover the secret message!
    Try using map and reduce together to accomplish this task.
    ex. decode("361581732726247521644353 4161492813593986955 84654117917337166147521") //
    => "abc"
    ex. decode("584131398786538461382741 444521974525439455955
    71415168525426614834414214 353238892594759181769 48955328774167683152
    77672648114592331981342373 5136831421236 83269359618185726749 2554892676446686256
    959958531366848121621517 4275965243664397923577 616142753591841179359
    121266483532393851149467 17949678591875681") // => "secret-message"
    var decode = function(string){
    // your code here
    } 
*/

var decode = function (string){
    // Split string into array of words (sets of numbers)
    const arrayOfWords = string.split(" ");

    // Split each word in to its own array of letters, which are inside of one single array
    const arraysOfLetters = arrayOfWords.map(word => word.split(""));
    
    // From each array (of letters of numbers), count the sum of numbers. Create new array for sums.
    // Numbers are in letter form, so they have to be parsed to integers.
    const arrayOfSums = arraysOfLetters.map(array => array.reduce((total, currValue) => total + parseInt(currValue), 0));
    
    // Each sum is ascii code for a letter => create word from the letters
    const decodedMessage = arrayOfSums.reduce((word, char) => word + String.fromCharCode(char), "");

    return decodedMessage;
} 

const msg = decode("584131398786538461382741 444521974525439455955 71415168525426614834414214 353238892594759181769 48955328774167683152 77672648114592331981342373 5136831421236 83269359618185726749 2554892676446686256 959958531366848121621517 4275965243664397923577 616142753591841179359 121266483532393851149467 17949678591875681");
console.log(msg);