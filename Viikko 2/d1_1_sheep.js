/*
    Count sheeps

    Create a program that takes in a number from the command line, for example node .\countSheep.js 3 and 
    prints a string "1 sheep...2 sheep...3 sheep..."
*/

// Convert argv string to integer
const sheepAmount = parseInt(process.argv[2]);
let stringOutput = "";
for (let i = 1; i <= sheepAmount; i++) {
    stringOutput += i + " sheep...";
}

console.log(stringOutput);