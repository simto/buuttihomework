/*
    Ordinal numbers
    You have two arrays:
    const competitors = ['Julia', "Mark", "Spencer", "Ann" , "John", "Joe"]; const ordinals = ['st', 'nd', 'rd', 'th'];
    Create program that outputs competitors placements with following way: ['1st competitor was Julia', '2nd competitor was Mark', '3rd competitor
    was Spencer', '4th competitor was Ann', '5th competitor was John', '6th competitor was Joe']
*/

const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"]; 
const competitors2 = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe", "Julia", "Mark", "Spencer", "Ann" , "John", "Joe", "Julia", "Mark", "Spencer", "Ann" , "John", "Joe", "Julia", "Mark", "Spencer", "Ann" , "John", "Joe", "Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

// Map competitors: järjestysluvun mukaan kullekin oma teksti etuliitteeksi
//      - määritä järjestysnumero
//      - liitä järjestysnumeron mukainen arvo ordinals-taulusta, plus teksti
//          - järjestysnumeron oikeimman puoleisen numeron mukaan ordinal, toimii myös 21,22,101,102,...


// Create ordinal for given integer. Return string.
function ordinalForNumber (int) {
    // Calculate modulos => get last 1 and last 2 digits of given int
    let j = int % 10;
    let k = int % 100;
    // Ordinal is "st", when last digit is 1, except when last two digits are 11 (eleventh)
    if (j === 1 && k !== 11) {
        return int + ordinals[0];
    }
    // Ordinal is "nd", when last digit is 2, except when last two digits are 12 (twelfth)
    if (j === 2 && k !== 12) {
        return int + ordinals[1];
    }
    // Ordinal is "rd", when last digit is 3, except when last two digits are 13 (thirteenth)
    if (j === 3 && k !== 13) {
        return int + ordinals[2];
    }
    return int + ordinals[3];
  
/*    
    // Get last digit or given number from modulo (does not take into account 11, 12, 13 (th))
    let lastDigit = int % 10;
    // 0, 4, 5, 6, 7, 8, 9 all has same ordinal as 4 ("th")
    if ((lastDigit === 0) || (lastDigit >= 4)) {
        lastDigit = 4;
    }
    return ordinals[lastDigit-1];
*/
}

// Create output for results, from array of ordered competitors
function printResults (competitors) {
    const resultsArray = competitors.map((competitor, index) => {
        // Create result string for each competitor
        return `${ordinalForNumber(index + 1)} competitor was ${competitor}`;
    });
    return resultsArray;
}

console.log(printResults(competitors));
console.log(printResults(competitors2));