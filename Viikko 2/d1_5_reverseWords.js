/*
    Reverse words in sentence
    Create a programs that reverses each word in a string.
    node .\reverseWords.js "this is a very long sentence" -> sihT si a yrev gnol ecnetnes
*/

// Get sentence string as argument
const sentence = process.argv[2];
// Split given string in to array of words.
const words = sentence.split(" ");

// Function to reverse a given string
function reverseWord (word) {
    // 1. split each word into array, 2. reverse array, 3. join array into string
    return word.split("").reverse().join("");
}

// Map word array: reverse each word
const reversedWords = words.map(word => reverseWord(word));

console.log(reversedWords.join(" "));
