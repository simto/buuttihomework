"use strict";
import { v4 as uuidv4 } from "uuid";
import fs from "fs";

let books = [];

function updateJsonFile() {
    fs.writeFileSync("db.json", JSON.stringify(books), "utf-8");
}

export function readJsonFile() {
    try {
        books = JSON.parse(fs.readFileSync("db.json", "utf-8"));
    } catch {
        books = [];
    }
}

export const getAllBooks = (req, res) => {
    res.send(books);
};

export const getBook = (req, res) => {
    const book = books.find(book => book.id === req.params.id);
    res.send(book);
};

export const postBook = (req, res) => {
    const book = req.body;
    book.id = uuidv4();
    books = books.concat(req.body);
    updateJsonFile();
    res.redirect("/");
};

export const putBook = (req, res) => {
    const newBook = {...req.body, id: req.params.id};
    /*
    const newBook = req.body;
    newBook.id = req.params.id;
    */
    
    /* books = books.map(book => 
        book.id === req.params.id ? newBook : book
    ); */
    
    books = books.map(book => {
        if(book.id === req.params.id) {
            return newBook;
        } else {
            return book;
        }
    });
    updateJsonFile();
    res.redirect("/");
};

export const deleteBook = (req, res) => {
    books = books.filter(book => book.id !== req.params.id);
    updateJsonFile();
    res.redirect("/");
};