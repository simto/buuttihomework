"use strict";
import express from "express";
import { readJsonFile } from "./controllers/books.js";
import bookRouter from "./routes/books.js";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(express.static("public"));

app.use("/books", bookRouter);

readJsonFile();
app.listen(5000, () => {
    console.log("Listening to 5000");
});