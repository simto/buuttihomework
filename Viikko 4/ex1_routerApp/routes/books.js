"use strict";
import express from "express";
import * as BookController from "../controllers/books.js";

const router = express.Router();

/* Book includes id, name, author, read (boolean) */
router.get("/", BookController.getAllBooks);
router.get("/:id", BookController.getBook);
router.post("/", BookController.postBook);
router.put("/:id", BookController.putBook);
router.delete("/:id", BookController.deleteBook);

export default router;