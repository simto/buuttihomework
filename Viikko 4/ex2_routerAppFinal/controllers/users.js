"use strict";
import bcrypt from "bcrypt";
import { v4 as uuidv4 } from "uuid";
import fs from "fs";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config();

let users = [];
const saltRounds = 10;

function updateUsersJsonFile() {
    fs.writeFileSync("users.json", JSON.stringify(users), "utf-8");
}

export function readUsersJsonFile() {
    try {
        users = JSON.parse(fs.readFileSync("users.json", "utf-8"));
    } catch {
        users = [];
    }
}

/**
 * Gets all usernames
 */
export const getAllUsers = (req, res) => {
    const userNames = [];
    users.forEach(user => userNames.push(user.username));
    res.send(userNames);
};

/**
 * Register a new user
 * - generates hashed password
 * - generates unique id
 */
export const registerUser = async (req, res) => {
    // Check if username already exists
    if (users.find(user => user.username === req.body.username)) {
        res.status(401).send("Username already in use");     
    } else {
        const user = req.body;
        const pwHash = await bcrypt.hash(req.body.password, saltRounds);
        user.password = pwHash;
        user.id = uuidv4();
        user.friends = [];
        users = users.concat(user);
        updateUsersJsonFile();
        res.status(200).send("User added");
    }
};

/**
 * User login
 * - creates token for user session
 */
export const loginUser = async (req, res) => {
    // Find correct user and password from users-table
    const user = users.filter(user => user.username === req.body.username);
    let correctPassword;  
    if (user.length > 0) {
        correctPassword = user[0].password;
    } else {
        res.status(401).send("Username not found");
    }
        
    const isCorrectPassword = await bcrypt.compare(req.body.password, correctPassword);
    if(isCorrectPassword) {
        const token = jwt.sign (
            { username : req.body.username },
            process.env.JWT_SECRET, // JWT_SECRET voi olla mikä vain
            { expiresIn: "1h"}
        );    
        res.status(200).send({token});
    } else {
        res.status(401).send("Invalid username or password");
    }
};

export { users };