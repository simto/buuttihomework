"use strict";
import { users } from "./users.js";
import { books } from "./books.js";

export const getFriendBooks = (req, res) => {
    // Get user from request
    const user = users.find(user => user.username === req.user.username);
    
    // Check if given parameter (friendname) matches users friends
    if (user.friends.includes(req.params.friendname)) {
        // if user has a friend by given name
        const friend = users.find(user => user.username === req.params.friendname);
        const friendsBooks = books.filter(book => book.ownerId === friend.id);
        res.send(friendsBooks);
    } else {
        res.status(401).send("Friend not found");
    }
};

export const getAllFriendBooks = (req, res) => {
    // Get user from request and names of that user's friends
    const user = users.find(user => user.username === req.user.username);
    const usersFriends = user.friends;

    let friend;
    let friendsBooks = [];

    // Go through friends
    usersFriends.forEach(friendName => {
        // etsi kaverin nimeä vastaava käyttäjä
        friend = users.find(user => user.username === friendName);
        // listaa user id:n kaikki kirjat
        friendsBooks = friendsBooks.concat(books.filter(book => book.ownerId === friend.id));
    });

    res.send(friendsBooks);
};