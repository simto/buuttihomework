"use strict";
import { v4 as uuidv4 } from "uuid";
import fs from "fs";
import { users } from "./users.js";

let books = [];

function updateJsonFile() {
    fs.writeFileSync("db.json", JSON.stringify(books), "utf-8");
}

/**
 * Get user id matching user of the request
 * @param {*} request - request 
 * @returns {string} userId - Id of the user
 */
const getUserId = (request) => {
    const user = users.filter(user => user.username === request.user.username);
    const userId = (user.length > 0) ? user[0].id : null;
    return userId;
};

export function readJsonFile() {
    try {
        books = JSON.parse(fs.readFileSync("db.json", "utf-8"));
    } catch {
        books = [];
    }
}

/* Book includes id, name, author, read (boolean) */
// TESTING: sends all books, no user id checks
export const getAllBooksAdmin = (req, res) => {
    res.send(books);
};

/**
 * Get all books from library
 * Books' ownerID has to match id of the user making the request
 */
export const getAllBooks = (req, res) => {
    let userId = getUserId(req);

    if (userId === null) {
        res.status(401).send("Invalid user");
    }
    
    const booksByUserId = books.filter(book => book.ownerId === userId);
    res.send(booksByUserId);
};

/**
 * Get a book by its ID from library 
 * Book's ownerID has to match id of the user making the request.
 */
export const getBook = (req, res) => {
    let userId = getUserId(req);

    if (userId === null) {
        res.status(401).send("Invalid user");
    }
    
    let book = books.find(book => book.id === req.params.id);
    book = (book && book.ownerId === userId) ? book : [];
    res.send(book);
};

/**
 * Add a new book to library with ownerID
 */
export const postBook = (req, res) => {
    let userId = getUserId(req);
    
    if (userId === null) {
        res.status(401).send("User not found");
    }

    // Create new book 
    const book = req.body;
    book.id = uuidv4();
    book.ownerId = userId;
    books = books.concat(req.body);
    updateJsonFile();
    res.redirect("/");
};

/**
 * Modifies a book
 * Book's ownerID has to match id of the user making the request.
 */
export const putBook = (req, res) => {
    let userId = getUserId(req);
    
    if (userId === null) {
        res.status(401).send("User not found");
    }
        
    const newBook = {...req.body, id: req.params.id, ownerId: userId};
    
    books = books.map(book => {
        if(book.id === req.params.id && book.ownerId === userId) {
            return newBook;
        } else {
            return book;
        }
    });
    updateJsonFile();
    res.redirect("/");
};

/**
 * Modifies a book
 * Book's ownerID has to match id of the user making the request.
 */
export const deleteBook = (req, res) => {
    let userId = getUserId(req);
    
    if (userId === null) {
        res.status(401).send("User not found");
    }
    
    books = books.filter(book => !(book.id === req.params.id && book.ownerId === userId));
    updateJsonFile();
    res.redirect("/");
};

export { books };