"use strict";
import { users } from "./users.js";
import fs from "fs";

function updateUsersJsonFile() {
    fs.writeFileSync("users.json", JSON.stringify(users), "utf-8");
}

/**
 * Adds a friend to user
 */
export const postFriend = (req, res) => {
    const user = users.find(user => user.username === req.user.username);
    
    if (user.friends.find(friend => friend === req.body.friendname)) {
        res.status(401).send("Friend already added");
    } else {
        user.friends.push(req.body.friendname);
        updateUsersJsonFile();
        res.status(200).send("Friend added");
    }
};

export const getAllFriends = (req, res) => {
    const user = users.find(user => user.username === req.user.username);
    res.send(user.friends);
};