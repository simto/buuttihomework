"use strict";
import express from "express";
import * as BookController from "../controllers/books.js";
import * as UsersController from "../controllers/users.js";
import * as FriendsController from "../controllers/friends.js";
import * as FriendBooksController from "../controllers/friendbooks.js";
import jwt from "jsonwebtoken";

const router = express.Router();

// For testing purposes. Lists all books without need of authentication
router.get("/admin", BookController.getAllBooksAdmin);

// Register and login are allowed without authentication
router.post("/register", UsersController.registerUser);
router.post("/login", UsersController.loginUser);

router.use((req, res, next) => {
    const auth = req.get("authorization");
    if (auth && auth.toLowerCase().startsWith("bearer ")) {
        const token = auth.substring(7);
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
        if(!token || !decodedToken.username) {
            return res.status(401).json({error: "token missing or invalid"});
        }
        req.user = decodedToken;
        next();
    } else {
        return res.status(401).json({error: "token missing or invalid"});
    }
});

router.get("/books", BookController.getAllBooks);
router.get("/books/:id", BookController.getBook);
router.post("/books", BookController.postBook);
router.put("/books/:id", BookController.putBook);
router.delete("/books/:id", BookController.deleteBook);

router.get("/users", UsersController.getAllUsers);

router.get("/friends", FriendsController.getAllFriends);
router.post("/friends", FriendsController.postFriend);

router.get("/friendbooks", FriendBooksController.getAllFriendBooks);
router.get("/friendbooks/:friendname", FriendBooksController.getFriendBooks);

export default router;