"use strict";
import express from "express";
import { readJsonFile } from "./controllers/books.js";
import { readUsersJsonFile } from "./controllers/users.js";
import router from "./routes/router.js";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// app.use(express.static("public"));

app.use(router);

readJsonFile(); // Load Books
readUsersJsonFile(); // Load users

app.listen(5000, () => {
    console.log("Listening to 5000");
});